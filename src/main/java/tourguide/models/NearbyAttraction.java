package tourguide.models;

import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;

public class NearbyAttraction {

    private String attractionName;

    private Attraction attraction;

    private VisitedLocation userLocation;

    private double distance;

    private int rewardPoints;

    public NearbyAttraction(String attractionName, Attraction attraction, VisitedLocation userLocation, double distance, int rewardPoints) {
        this.attractionName = attractionName;
        this.attraction = attraction;
        this.userLocation = userLocation;
        this.distance = distance;
        this.rewardPoints = rewardPoints;
    }
}
