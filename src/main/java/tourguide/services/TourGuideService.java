package tourguide.services;

import gpsUtil.GpsUtil;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import tourguide.helper.InternalTestHelper;
import tourguide.models.AttractionDistance;
import tourguide.models.NearbyAttraction;
import tourguide.models.Tracker;
import tourguide.models.User;
import tourguide.models.UserPreferences;
import tourguide.models.UserReward;
import tripPricer.Provider;
import tripPricer.TripPricer;

import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class TourGuideService {
    /**********************************************************************************
     * Methods Below: For Internal Testing
     **********************************************************************************/
    private static final String TRIP_PRICER_API_KEY = "test-server-api-key";
    public final Tracker tracker;
    private final GpsUtil gpsUtil;
    private final RewardsService rewardsService;
    private final TripPricer tripPricer = new TripPricer();
    // Database connection will be used for external users, but for testing purposes internal users are provided and stored in memory
    private final ConcurrentHashMap<String, User> internalUserMap = new ConcurrentHashMap<>();
    boolean testMode = true;
    private final Logger logger = LoggerFactory.getLogger(TourGuideService.class);

    private final ExecutorService executorService;


    public TourGuideService(GpsUtil gpsUtil, RewardsService rewardsService) {
        this.gpsUtil = gpsUtil;
        this.rewardsService = rewardsService;
        this.executorService = Executors.newFixedThreadPool(50);

        if (testMode) {
            logger.info("TestMode enabled");
            logger.debug("Initializing users");
            initializeInternalUsers();
            logger.debug("Finished initializing users");
        }
        tracker = new Tracker(this);
        addShutDownHook();
    }

    public List<UserReward> getUserRewards(User user) {
        return user.getUserRewards();
    }

    public VisitedLocation getUserLocation(User user) throws ExecutionException, InterruptedException {
        return (!user.getVisitedLocations().isEmpty()) ?
                user.getLastVisitedLocation() :
                trackUserLocation(user).get();
    }

    public Map<UUID, Location> getAllCurrentLocations() {
        Map<UUID, Location> allCurrentLocation = new HashMap<>();
        List<User> users = getAllUsers();
        users.forEach(user -> allCurrentLocation.put(user.getUserId(), user.getLastVisitedLocation().location));
        return allCurrentLocation;
    }

    public User getUser(String userName) {
        return internalUserMap.get(userName);
    }

    public List<User> getAllUsers() {
        return internalUserMap.values().stream().parallel().collect(Collectors.toList());
    }

    public void addUser(User user) {
        if (!internalUserMap.containsKey(user.getUserName())) {
            internalUserMap.put(user.getUserName(), user);
        }
    }

    public List<Provider> getTripDeals(User user) {
        int cumulatativeRewardPoints = user.getUserRewards().stream().mapToInt(UserReward::getRewardPoints).sum();
        List<Provider> providers = tripPricer.getPrice(TRIP_PRICER_API_KEY, user.getUserId(), user.getUserPreferences().getNumberOfAdults(),
                user.getUserPreferences().getNumberOfChildren(), user.getUserPreferences().getTripDuration(), cumulatativeRewardPoints);
        user.setTripDeals(providers);
        return providers;
    }

    public CompletableFuture<VisitedLocation> trackUserLocation(User user) {
        CompletableFuture<VisitedLocation> runner =
                CompletableFuture.supplyAsync(() -> gpsUtil.getUserLocation(user.getUserId()), executorService);
        runner.thenAcceptAsync(user::addToVisitedLocations)
                .thenRun(() -> rewardsService.calculateRewards(user));
        return runner;
    }

    public List<NearbyAttraction> getNearByAttractions(VisitedLocation visitedLocation, User user) {
        List<NearbyAttraction> nearbyAttractions = new ArrayList<>();
        List<AttractionDistance> bufferList = new ArrayList<>();
        CompletableFuture<Void> calculateNearbyAttraction = CompletableFuture.supplyAsync(gpsUtil::getAttractions)
                .thenAccept(attractions -> {
                    attractions.forEach(attraction -> {
                        double distance = rewardsService.getDistance(visitedLocation.location, attraction);
                        bufferList.add(new AttractionDistance(attraction, distance));
                    });
                    bufferList.sort(Comparator.comparing(AttractionDistance::getDistance));
                }).thenRun(() ->
                        bufferList.subList(0, 5).forEach(attractionDistance -> nearbyAttractions.add(
                                new NearbyAttraction(attractionDistance.getAttraction().attractionName,
                                        attractionDistance.getAttraction(), visitedLocation, attractionDistance.getDistance(),
                                        rewardsService.getRewardPoints(attractionDistance.getAttraction(), user)))
                        ));
        try {
            while (!calculateNearbyAttraction.isDone()) {
                TimeUnit.MILLISECONDS.sleep(2);
            }
            calculateNearbyAttraction.get();
        } catch (InterruptedException | ExecutionException e) {
            logger.error("Error while getting nearbyAttractions", e);
            Thread.currentThread().interrupt();
        }
        return nearbyAttractions;
    }

    public void shutdownExecutorService() {
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(10, TimeUnit.SECONDS)) {
                executorService.shutdownNow();
            }
        } catch (InterruptedException e) {
            logger.error("Interrupted Exception during executor shutdown", e);
            Thread.currentThread().interrupt();
        }
    }

    public UserPreferences setUserPreferences(String userName, String numberOfAdult, String numberOfChilden, String tripDuration) {
        UserPreferences userPreferences = new UserPreferences(Integer.parseInt(tripDuration), Integer.parseInt(numberOfAdult), Integer.parseInt(numberOfChilden));
        getUser(userName).setUserPreferences(userPreferences);
        return userPreferences;
    }

    private void addShutDownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(tracker::stopTracking));
    }

    private void initializeInternalUsers() {
        IntStream.range(0, InternalTestHelper.getInternalUserNumber()).forEach(i -> {
            String userName = "internalUser" + i;
            String phone = "000";
            String email = userName + "@tourGuide.com";
            User user = new User(UUID.randomUUID(), userName, phone, email);
            generateUserLocationHistory(user);

            internalUserMap.put(userName, user);
        });
        logger.debug("Created {} internal test users", InternalTestHelper.getInternalUserNumber());
    }

    private void generateUserLocationHistory(User user) {
        IntStream.range(0, 3).forEach(i -> user.addToVisitedLocations(new VisitedLocation(user.getUserId(), new Location(generateRandomLatitude(), generateRandomLongitude()), getRandomTime())));
    }

    private SecureRandom getRandom() {
        return new SecureRandom();
    }

    private double generateRandomLongitude() {
        double leftLimit = -180;
        double rightLimit = 180;
        return leftLimit + getRandom().nextDouble() * (rightLimit - leftLimit);
    }

    private double generateRandomLatitude() {
        double leftLimit = -85.05112878;
        double rightLimit = 85.05112878;
        return leftLimit + getRandom().nextDouble() * (rightLimit - leftLimit);
    }

    private Date getRandomTime() {
        LocalDateTime localDateTime = LocalDateTime.now().minusDays(getRandom().nextInt(30));
        return Date.from(localDateTime.toInstant(ZoneOffset.UTC));
    }


}
