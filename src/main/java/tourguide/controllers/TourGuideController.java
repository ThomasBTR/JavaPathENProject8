package tourguide.controllers;

import com.jsoniter.output.JsonStream;
import gpsUtil.location.VisitedLocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tourguide.models.User;
import tourguide.models.UserPreferences;
import tourguide.services.TourGuideService;
import tripPricer.Provider;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

@RestController
public class TourGuideController {

    @Autowired
    TourGuideService tourGuideService;

    private final Logger logger = LoggerFactory.getLogger(TourGuideController.class);

    @GetMapping("/")
    public String index() {
        return "Greetings from TourGuide!";
    }

    @GetMapping("/location")
    public String getLocation(@RequestParam String userName) {
        VisitedLocation visitedLocation = null;
        try {
            visitedLocation = tourGuideService.getUserLocation(getUser(userName));
        } catch (ExecutionException | InterruptedException e) {
            logger.error("Error while getting userLocation for user {}", userName, e);
            Thread.currentThread().interrupt();
        }
        return JsonStream.serialize(Objects.requireNonNull(visitedLocation).location);
    }

    @GetMapping("/nearbyAttractions")
    public String getNearbyAttractions(@RequestParam String userName) {
        VisitedLocation visitedLocation = null;
        try {
            visitedLocation = tourGuideService.getUserLocation(getUser(userName));
        } catch (ExecutionException | InterruptedException e) {
            logger.error("Error while getting nearbyAttraction for user {}", userName, e);
            Thread.currentThread().interrupt();
        }
        return JsonStream.serialize(tourGuideService.getNearByAttractions(visitedLocation, getUser(userName)));
    }

    @GetMapping("/rewards")
    public String getRewards(@RequestParam String userName) {
        return JsonStream.serialize(tourGuideService.getUserRewards(getUser(userName)));
    }

    @GetMapping("/allCurrentLocations")
    public String getAllCurrentLocations() {
        return JsonStream.serialize(tourGuideService.getAllCurrentLocations());
    }

    @GetMapping("/tripDeals")
    public String getTripDeals(@RequestParam String userName) {
        List<Provider> providers = tourGuideService.getTripDeals(getUser(userName));
        return JsonStream.serialize(providers);
    }

    @PostMapping("/userPreference")
    public String setUserPreference(@RequestParam String userName, @RequestParam String numberOfChilden,
                                    @RequestParam String numberOfAdult, @RequestParam String tripDuration) {
        UserPreferences userPreferences = tourGuideService.setUserPreferences(userName, numberOfAdult, numberOfChilden, tripDuration);
        return JsonStream.serialize(userPreferences);
    }

    private User getUser(String userName) {
        return tourGuideService.getUser(userName);
    }


}