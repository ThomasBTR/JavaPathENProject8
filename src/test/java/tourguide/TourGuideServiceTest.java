package tourguide;

import gpsUtil.GpsUtil;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import rewardCentral.RewardCentral;
import tourguide.helper.InternalTestHelper;
import tourguide.models.NearbyAttraction;
import tourguide.models.User;
import tourguide.services.RewardsService;
import tourguide.services.TourGuideService;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;
import static org.junit.Assert.assertEquals;

@SpringBootTest
public class TourGuideServiceTest {

    @Test
    public void getUserLocation() {
        Locale.setDefault(Locale.ENGLISH);
        GpsUtil gpsUtil = new GpsUtil();
        RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
        InternalTestHelper.setInternalUserNumber(0);
        TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService);
        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        VisitedLocation visitedLocation = null;
        try {
            visitedLocation = tourGuideService.trackUserLocation(user).get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
        tourGuideService.tracker.stopTracking();
        assertThat(visitedLocation.userId).isEqualTo((user.getUserId()));
    }

    @Test
    public void getAllCurrentLocations() {
        //GIVEN
        Locale.setDefault(Locale.ENGLISH);
        GpsUtil gpsUtil = new GpsUtil();
        RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
        InternalTestHelper.setInternalUserNumber(10);
        TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService);
        //WHEN
        Map<UUID, Location> allCurrentLocation = tourGuideService.getAllCurrentLocations();
        //THEN
        assertThat(allCurrentLocation).hasSameSizeAs(tourGuideService.getAllUsers());

    }

    @Test
    public void addUser() {
        GpsUtil gpsUtil = new GpsUtil();
        RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
        InternalTestHelper.setInternalUserNumber(0);
        TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService);
        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        User user2 = new User(UUID.randomUUID(), "jon2", "000", "jon2@tourGuide.com");
        tourGuideService.addUser(user);
        tourGuideService.addUser(user2);
        User retrivedUser = tourGuideService.getUser(user.getUserName());
        User retrivedUser2 = tourGuideService.getUser(user2.getUserName());
        tourGuideService.tracker.stopTracking();
        assertEquals(user, retrivedUser);
        assertEquals(user2, retrivedUser2);
    }

    @Test
    public void getAllUsers() {
        GpsUtil gpsUtil = new GpsUtil();
        RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
        InternalTestHelper.setInternalUserNumber(0);
        TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService);
        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        User user2 = new User(UUID.randomUUID(), "jon2", "000", "jon2@tourGuide.com");
        tourGuideService.addUser(user);
        tourGuideService.addUser(user2);
        List<User> allUsers = tourGuideService.getAllUsers();
        tourGuideService.tracker.stopTracking();
        assertThat(allUsers).contains(user).contains(user2);
    }

    @Test
    public void trackUser() {
        Locale.setDefault(Locale.ENGLISH);
        GpsUtil gpsUtil = new GpsUtil();
        RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
        InternalTestHelper.setInternalUserNumber(0);
        TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService);
        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        VisitedLocation visitedLocation;
        try {
            visitedLocation = tourGuideService.trackUserLocation(user).get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
        tourGuideService.tracker.stopTracking();
        assertThat(user.getUserId()).isEqualTo(visitedLocation.userId);
    }

    @Test
    public void getNearbyAttractions() {
        Locale.setDefault(Locale.ENGLISH);
        GpsUtil gpsUtil = new GpsUtil();
        RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
        InternalTestHelper.setInternalUserNumber(0);
        TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService);
        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        tourGuideService.addUser(user);
        VisitedLocation visitedLocation;
        CompletableFuture<VisitedLocation> completableFuture = tourGuideService.trackUserLocation(tourGuideService.getAllUsers().get(0));
        try {
            await().atMost(60, TimeUnit.SECONDS).until(completableFuture::isDone);
            visitedLocation = completableFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
        List<NearbyAttraction> attractions = tourGuideService.getNearByAttractions(visitedLocation, tourGuideService.getAllUsers().get(0));
        tourGuideService.tracker.stopTracking();
        assertThat(attractions).hasSize(5);
    }
}
