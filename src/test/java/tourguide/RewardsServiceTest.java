package tourguide;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import rewardCentral.RewardCentral;
import tourguide.helper.InternalTestHelper;
import tourguide.models.User;
import tourguide.models.UserReward;
import tourguide.services.RewardsService;
import tourguide.services.TourGuideService;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;


@SpringBootTest
public class RewardsServiceTest {


    @Test
    public void userGetRewards() {
        Locale.setDefault(Locale.ENGLISH);
        GpsUtil gpsUtil = new GpsUtil();
        RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());

        InternalTestHelper.setInternalUserNumber(0);
        TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService);

        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        Attraction attraction = gpsUtil.getAttractions().get(0);
        user.addToVisitedLocations(new VisitedLocation(user.getUserId(), attraction, new Date()));
        tourGuideService.trackUserLocation(user);
        await().atMost(10, TimeUnit.SECONDS).until(() -> !user.getUserRewards().isEmpty());
        List<UserReward> userRewards = user.getUserRewards();
        tourGuideService.tracker.stopTracking();
        assertThat(userRewards).hasSize(1);
    }

    @Test
    public void isWithinAttractionProximity() {

        GpsUtil gpsUtil = new GpsUtil();
        RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
        Attraction attraction = gpsUtil.getAttractions().get(0);
        assertThat(rewardsService.isWithinAttractionProximity(attraction, attraction)).isTrue();
    }

    // Needs fixed - can throw ConcurrentModificationException
    @Test
    public void nearAllAttractions() {

        //GIVEN
        Locale.setDefault(Locale.ENGLISH);
        GpsUtil gpsUtil = new GpsUtil();
        RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
        rewardsService.setProximityBuffer(Integer.MAX_VALUE);
        List<Attraction> gpsUtilAttractions = gpsUtil.getAttractions();
        InternalTestHelper.setInternalUserNumber(1);
        TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService);
        User user = tourGuideService.getAllUsers().get(0);

        //WHEN
        rewardsService.calculateRewards(user);
        await().atMost(10, TimeUnit.SECONDS).until(() -> tourGuideService.getAllUsers().get(0).getUserRewards().size() == gpsUtilAttractions.size());

        //THEN
        User userAfterRewards = tourGuideService.getAllUsers().get(0);
        List<UserReward> userRewards = tourGuideService.getUserRewards(userAfterRewards);
        tourGuideService.tracker.stopTracking();

        assertThat(gpsUtilAttractions).hasSameSizeAs(userRewards);
    }

}
